const fs = require('fs')
const cmd = require('node-cmd')

const modulesPath = '../modules/userModules'
const jsonListsPath = '../jsonLists/doe06'

const createUser = require(modulesPath + '/createUser.js')
const connectUser = require(modulesPath + '/connectUser.js')
const editUser = require(modulesPath + '/editUser.js')
const createGroup = require('../modules/groupModules/createGroup.js')

let settings = fs.readFileSync('../createUserDefaultSetting.json')
settings = JSON.parse(settings)
let users = fs.readFileSync(jsonListsPath + '/users.json')
users = JSON.parse(users)
let groups = fs.readFileSync(jsonListsPath + '/groups.json')
groups = JSON.parse(groups)

let startTime
let endTime 

Date.now = () => { return new Date().getTime(); }
startTime = Date.now()/1000
startTime = Math.round(startTime)
console.log(startTime)

let id = Math.floor((Math.random() * (3999))+ 1000)
//Run through users.json and use that data to create then modify the users
users.forEach((user) => {
    //Create

    console.log(createUser(user.name, id, 'TEST', settings))
    cmd.run(createUser(user.name, id, 'TEST', settings))
    id++

    //Modify
    //ALTUSER  UISPZ01       PASSWORD(NEWPASSW) NOEXPIRED
    console.log(createUser(user.id, 'PASSWORD', 'NEWPASSW', 'NOEXPIRED'))
    cmd.run(createUser(user.id, 'PASSWORD', 'NEWPASSW', 'NOEXPIRED'))

    console.log('')
})


console.log('')
console.log('')

//Create groups- for id, use i
let i = Math.floor((Math.random() * (4999))+ 4000)
i += 1000

console.log('i = ' + i)

groups.forEach((collection) => {
    collection.groups.forEach((group) =>{
        if(group.name === 'GIS'){
            i++
            console.log(createGroup(group.name, 'UISDB01', i))
            cmd.run(createGroup(group.name, 'UISDB01', i))
            return 
        }
        i++

        console.log(createGroup(group.name, 'GIS', i, 'GIS'))
        cmd.run(createGroup(group.name, 'GIS', i, 'GIS'))        
    })
})

console.log('')
console.log('')

users.forEach((user) => {
    console.log("Connecting the user. " + user + "'s collection is: " + user.group)
    cmd.run(connectUser.connectUser(user.name, connectUser.findGroup(user.group)))
})

Date.now = () => { return new Date().getTime(); }
endTime = (Date.now()/1000)
endTime = Math.round(endTime)
console.log(endTime)

console.log(endTime-startTime)
