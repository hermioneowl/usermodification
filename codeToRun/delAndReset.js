const fs = require('fs')
const cmd = require('node-cmd')

const modulesPath = '../modules/resetModules'
const jsonListsPath = '../jsonLists/doe06'

const listThenDelete = require(modulesPath + '/listThenDelete.js')

let users = fs.readFileSync(jsonListsPath + '/users.json')
users = JSON.parse(users)
let groups = fs.readFileSync(jsonListsPath + '/groups.json')
groups = JSON.parse(groups)

let userDelList = []
let groupDelList = []

let startTime
let endTime

Date.now = () => { return new Date().getTime(); }
startTime = Math.round(Date.now()/1000)
console.log(startTime)

//Add all users to a group for deletion
users.forEach((user) => {
    userDelList.push(user.name)
})

//Add groups to the list for deletion
groups.forEach((collection) => {
    collection.groups.forEach((group) =>{
        groupDelList.unshift(group.name)
    })
})

//Delete all of the users
while(userDelList.length > 0){
    listThenDelete(userDelList[0], userDelList, 'user')
    userDelList.shift()
}

//Delete all of the groups
while(groupDelList.length > 0){
    listThenDelete(groupDelList[0], groupDelList, 'group')
    groupDelList.shift()
}

Date.now = () => { return new Date().getTime(); }
endTime = Math.round(Date.now()/1000)
console.log(endTime)

console.log(endTime-startTime)