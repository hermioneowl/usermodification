const fs = require('fs')
const cmd = require('node-cmd')

const modulesPath = '../modules/resetModules'
const jsonListsPath = '../jsonLists/doe06'

const list = require(modulesPath + '/list.js')

let users = fs.readFileSync(jsonListsPath + '/users.json')
users = JSON.parse(users)
let groups = fs.readFileSync(jsonListsPath + '/groups.json')
groups = JSON.parse(groups)

let userList = []
let groupList = []

let startTime
let endTime

Date.now = () => { return new Date().getTime(); }
startTime = Math.round(Date.now()/1000)
console.log(startTime)


//Add all users to a group for deletion
users.forEach((user) => {
    userList.push(user.name)
})

//Add groups to the list for deletion
groups.forEach((collection) => {
    collection.groups.forEach((group) =>{
        groupList.unshift(group.name)
    })
})

//List all of the users
while(userList.length > 0){
    list(userList[0], 'user')
    userList.shift()
}

//List Groups
while(groupList.length > 0){
    list(groupList[0], 'group')
    groupList.shift()
}


Date.now = () => { return new Date().getTime(); }
endTime = Math.round(Date.now()/1000)
console.log(endTime)

console.log(endTime-startTime)