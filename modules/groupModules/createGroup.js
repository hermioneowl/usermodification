const createGroup = (name, owner, id, supgrp) => {
    let msg
    msg = "tsocmd 'ADDGROUP (" + name + ")" 
    msg += " OWNER(" + owner + ")" 
    msg += " OMVS(GID(" + id + "))"
    if(supgrp){
        msg += " SUPGROUP(" + supgrp + ")"
    }
    msg += "'"
    
    return msg

}
module.exports = createGroup