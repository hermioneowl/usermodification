const cmd = require('node-cmd')

list = (thingToList, listType) => {
    //Creates the list command
    let list
    if(listType === 'group'){
        list = "tsocmd 'Listgrp " + thingToList + "'"
    } else if (listType === 'user'){
        list = "tsocmd 'Listuser " + thingToList + "'"
    } else{
        return console.log('Invalid deletion type: Replace with user or group')
    }
    
    //Actually lists the user/group
    cmd.get(
        list,
        (err, data, stderr) =>{
            if(err){
                return console.log(thingToList + ' does not exist')
            }
            console.log(listType + ': ' + thingToList)
            //console.log(data)
        }
    )
}

module.exports = list