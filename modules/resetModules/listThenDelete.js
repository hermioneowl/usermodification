const cmd = require('node-cmd')

listThenDelete = (thingToDel, delList, deletionType) => {
    //Creates the list command
    let list
    if(deletionType === 'group'){
        list = "tsocmd 'Listgrp " + thingToDel + "'"
    } else if (deletionType === 'user'){
        list = "tsocmd 'Listuser " + thingToDel + "'"
    } else{
        return console.log('Invalid deletion type: Replace with user or group')
    }
    
    //Actually lists the user/group
    cmd.get(
        list,
        (err, data, stderr) =>{
            if(err){
                return console.log(err)
            }
            console.log(deletionType + ': ' + thingToDel)
            //console.log(data)
        }
    )

    //Delete
    cmd.run("tsocmd 'Del" + deletionType + " " + thingToDel + "'")

    //List again to prove that it isn't there anymore
    cmd.get(
        list,
        (err, data, stderr) =>{
            if(err){
                return console.log('Deletion of ' + deletionType + ' ' + thingToDel + ' confirmed')
            }
            console.log('Error in the deletion of the ' + deletionType + ': ' + thingToDel)
            delList.push(thingToDel)
        }
    )
}
module.exports = listThenDelete


