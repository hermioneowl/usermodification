const fs = require('fs')
const cmd = require('node-cmd')

const contentsGroups = fs.readFileSync('../jsonLists/doe06/groups.json')

contentGroups = JSON.parse(contentsGroups)

findGroup = (givenGroup) => {
    i = 0
    contentGroups.forEach((group) =>{
        i++
        if(group.collectionName === givenGroup){
            groups = contentGroups[i-1]
        }
    })
    return groups
}

connectUser = (userID, groups) => {
    groups.groups.forEach((group) => {
        cmd.run("tsocmd '" + "CONNECT (" + userID + ") GROUP (" + group.name + ")'")
        console.log("Connecting " + userID + " to " + group.name)
    })

    //Checks to see if there are any other collections of groups to use, and if there are, then it takes the groups in that group
    //and puts the user in those groups too.
    if(groups.groupCollections){
        //Iterates once per groupCollection, as there may be multiple
        groups.groupCollections.forEach((groupCollection) => {
            //Shows the group included
            //Checks through all of the groups in the groups.json to find the proper group
            contentGroups.forEach((group) => {
                if(group.collectionName === groupCollection.name){
                    //Adds all groups within the groupCollection
                    group.groups.forEach((group) => {
                        cmd.run("tsocmd '" + "CONNECT (" + userID + ") GROUP (" + group.name + ")'")
                        console.log("Connecting " + userID + " to " + group.name)
                    })
                }
            })
        })
    }
}
module.exports = {
    connectUser,
    findGroup
}