createUser = (USERNAME, UID, TEMPLATENAME, settings) => {
    //Settings should be in json format
    
    let HOMEPATH
    let SHELLPATH = settings.SHELLPATH
    let PASSWORD = settings.PASSWORD
    let ACCTNUM = settings.ACCTNUM
    let COMMAND = settings.COMMAND
    let PROC = settings.PROC
    let SIZE = settings.SIZE
    let OUTPUTCLASS = settings.OUTPUTCLASS
    let OWNER = settings.OWNER
    let DFLTGRP = settings.DFLTGRP

    //Setup for tsocmd
    HOMEPATH = '/doeu/' + USERNAME.toLowerCase()
    let endCmd

    //Creating the tsocmd
    endCmd = 'tsocmd "ADDUSER (' + USERNAME + ') OMVS(UID(' + UID + ')'
    endCmd = endCmd + " HOME ('" + HOMEPATH + "')"
    endCmd = endCmd + ' PROGRAM(' + SHELLPATH + '))'
    endCmd = endCmd + ' PASSWORD(' + PASSWORD + ')'
    endCmd = endCmd + ' TSO( ACCTNUM(' + ACCTNUM + '   ) '
    endCmd = endCmd + "COMMAND(" + COMMAND + "    ) "
    endCmd = endCmd + "PROC(" + PROC + ") "
    endCmd = endCmd + "SIZE(" + SIZE + " ) "
    endCmd = endCmd + "SYS(" + OUTPUTCLASS + ")) "
    endCmd = endCmd + "OWNER(" + OWNER + ") "
    endCmd = endCmd + "DFLTGRP(" + DFLTGRP + ')" '

    return endCmd
}

module.exports = createUser

//tsocmd "ADDUSER (UISPZ01) OMVS(UID(2100) HOME ('/doeu/default') PROGRAM(/bin/sh) ) PASSWORD(LETMEIN ) TSO( ACCTNUM(ACCT#) COMMAND(ISPF) PROC(ISPFPROC) SIZE(512000 ) SYS(H) ) OWNER(IBMUSER) DFLTGRP(USERGRP)"


/* Error creating the users:
IBMUSER:/u/ibmuser/racf/usermodification/codeToRun: >tsocmd 'listuser UISTG02'
listuser UISTG02
ICH30001I UNABLE TO LOCATE USER    ENTRY UISTG02          
<F    ) PROC(ISPFPROC) SIZE(512000 ) SYS(H)) OWNER(IBMUSER) DFLTGRP(USERGRP)"     
ADDUSER (UISPD01) OMVS(UID(000003) HOME ('/doeu/uispd01') PROGRAM(/bin/sh)) PASSWORD(PASSWORD) TSO( ACCTNUM(ACCT#   ) COMMAND(ISPF    ) PROC(ISPFPROC) SIZE(512000 ) SYS(H)) OWNER(IBMUSER) DFLTGRP(USERGRP)
IKJ56702I INVALID USERID, UISPD01
IKJ56701I MISSING OMVS UID+
IKJ56701I MISSING OMVS USER ID (UID), 1-10 NUMERIC DIGITS 
*/