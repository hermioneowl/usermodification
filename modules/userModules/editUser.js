editUser = (UID, option, changedOption, anyAddedEditables) => {
    let cmd = "tsocmd '" + 'ALTUSER ' + UID + ' ' + option + '(' + changedOption + ')' + "'"
    if(option === 'PASSWORD' && anyAddedEditables ){
        cmd = cmd + ' ' + anyAddedEditables
    }
    return cmd
}

module.exports = editUser